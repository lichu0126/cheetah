CC=/usr/bin/gcc
#getconf LONG_BIT to get the BITS_PER_LONG
										#-march is for __sync_add_and_fetch 
CFLAGS=-g -Wall -O3 -DBITS_PER_LONG=32  -march=i686				 #for 32Bit OS
#CFLAGS=-g -Wall -O3 -DBITS_PER_LONG=32  -march=x86-64           #for 64bit OS
#CFLAGS=-g -Wall -DDBG
LDFLAGS=
LIBS=-lm -lpthread
HEADERS=$(wildcard *.h)
SOURCES=$(wildcard *.c)
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=cheetha

#all: $(SOURCES) $(EXECUTABLE)
all: $(EXECUTABLE)	

$(EXECUTABLE): $(OBJECTS) $(HEADERS) 
	$(CC) $(LDFLAGS) $(OBJECTS) $(LIBS) -o $@

install:
	/bin/cp $(EXECUTABLE) /usr/local/bin/

.PHONY: clean
clean:
	rm -f *.o ${EXECUTABLE}
