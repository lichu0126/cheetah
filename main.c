/*************************************************************************
    > File Name: main.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年10月30日 星期四 21时10分57秒
 ************************************************************************/

#include<stdio.h>
#include "libcheetah.h"

int trace_type;
FILE *in;

int main(int argc, char **argv )
{
	printf("call cheetah_init\n");
	cheetah_init(argc, argv);
#if 0
	cheetah_access(1);/* address of access */
	cheetah_access(2);/* address of access */
	cheetah_access(3);/* address of access */
	cheetah_access(2);/* address of access */
	cheetah_access(4);/* address of access */
	cheetah_access(3);/* address of access */
	cheetah_access(5);/* address of access */
	cheetah_access(1);/* address of access */
	cheetah_access(3);/* address of access */
	cheetah_access(2);/* address of access */
	cheetah_access(3);/* address of access */
	cheetah_access(5);/* address of access */
#endif

	int addr;
	in = fopen("/dev/shm/input","r");
	while (fscanf(in, "%d",&addr) == 1){
		addr/=8;
//		printf("addr=%d\n",addr);
		cheetah_access(addr);/* address of access */
	}
	fclose(in);

	cheetah_stats(stdout,0);
	return 0;
}
